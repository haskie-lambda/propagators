" Vim syntax file
" Language: prop
" Maintainer: Fabian Schneider
" Latest Revision: 2021-04-03

if exists("b:current_syntax")
    finish
endif

syn keyword constructKeywords prop mesh nextgroup=constructName skipwhite
syn match constructName '\(_|[a-z]\)[^ \s\t]' nextgroup=constructType skipwhite
syn keyword constructType omni uni nextgroup=paramRegion,namedParamRegion
syn region paramRegion start="{" end='}' fold transparent contains=Identifier nextgroup=constructBody skipwhite
syn match Identifier '\(_|[a-z]\)[^ \s\t]'

syn region constructBody start=":" end="\n\n" fold transparent contains=ParamMatch skipwhite
syn region ParamMatch contains=Identifier transparent nextgroup=arrow skipwhite
syn keyword arrow -> nextgroup=
