# SIMBA
"**SIM**ulated a**B**stract h**A**rdware" is a 
parallelism, reactive oriented interpreted language based on 
the concept of propagators.
It was created for the coures "Abstract Machines" at TU
Vienna in the summer semester 2021.

The aim was not to produce a complete, working, ready to use
language but to explore the implementation of hardware simulation
and reactive/dataflow paradigm languages.

The core idea was to enable massive parallelism, meaning that 
every part of a specified propagator-mesh (kind of like an
electronic circuit) can run independently in its' own thread,
no matter where it will be deployed.

This project ignored existing implementation approaches to propagators 
in functional languages using arrows or other abstractions and 
focused more on the interpreting und runtime-representation part.

A HTML-browsable documentation along with the source can be 
found [here](https://haskie-lambda.gitlab.io/propagators/)
and the presentation slides of the semester presentation 
[here](https://gitlab.com/haskie-lambda/propagators/-/blob/master/presentation.pdf).

Example programs can be found in the testfiles folder and in `lvl.prop`.
The script `buildOutputFor` generates a pretty printed abstract syntax tree
output for a filename in `testfiles` for inspection nad debugging huge files.

## Building
The project can be built and run using `cabal`
```sh
$> cabal build
$> cabal run
```
which will interpret `./testfiles/simple.prop`
or using `nix`:
```sh
$> nix-shell --command "cabal run" # or build respectively
```

## The Runtime
As SIMBA is a reactive language, there is not single thread of execution
the program follows when started.
All propagators start immediately and listen for inputs on their ports.
Executing a program will put the user into a REPL with a simple set of commands
(see the source in the `Languge` module for documentation):
it  is possible to set and retrieve the values in the cells the propagators listen
to at any time and it is possible to inspect the wiring diagram (via a list of 
UUIDs) and the complete runtime state.

Examples using these features will be added in time.

## Future Work and Contribution:
Contributions are welcome via issues and pull requests.
Future plans for the system are laid out in the project presentation.
