{
{-# LANGUAGE DeriveDataTypeable #-}
module Prop.Lexer where
import Data.Typeable
import Data.Data
import Data.List
import Data.List.Split
import Data.Char
import Debug.Trace
import Prelude hiding (lex)
import Control.Monad (liftM)
}

%wrapper "posn"

$digit            = 0-9
@string           = (. # [\" \\] )

$alpha            = [a-zA-Z]
@real             = ($digit+ \. | $digit * \. $digit +)
@boolLit          = ("True"|"False")
@alphaNum         = ($alpha|$digit)+
$bracketsOpen     = [\(\[\{]
$bracketsClose    = [\)\]\}]
$brackets         = [ $bracketsOpen $bracketsClose]
@identifier       = (. # [ : \; ! = \\ \ " $brackets])+
@extendedString   = (@string* "%" @identifier @string*)+ 
@commaOrSpace     = (\,\ * | \ +)
@scopedIdentifier = @identifier(\.@identifier)+
@globalKeyword    = (prop|mesh|let|omni|uni|let|using|module|import|where|qualified|as)
@indent           = $white* \n " "+

tokens :-
    @indent         { \a s -> PosToken a $ Indent $ length $ takeWhile (/='\n') $ reverse s }
    $white+         ;
    "--".*          ;
    @globalKeyword  { \a keyword -> PosToken a $ getTokenOf keyword }
    $digit+         { \a s -> PosToken a $ IntL (read s) }
    @real+          { \a s -> PosToken a $ DoubleL (read s) }
    @boolLit        { \a s -> PosToken a $ BoolL (read s) }
    \" @extendedString \" { \a s -> PosToken a $ lexExtendedString (tail . init $ s) }
    \" @string* \"  { \a s -> PosToken a $ StringLit (tail . init $ s) }
    ":"             { \a s -> PosToken a $ DefinedByCol }
    ";"             { \a s -> PosToken a $ Scol }
    ","             { \a s -> PosToken a $ Comma }
    "!"             { \a s -> PosToken a $ Negate }
    "=="            { \a s -> PosToken a $ Eq }
    "="             { \a s -> PosToken a $ LetAssOp }
    "~>"            { \a s -> PosToken a $ Wire }
    "->"            { \a s -> PosToken a $ PatternMatchEnd }
    $bracketsOpen   { \a s -> PosToken a $ BracO s}
    $bracketsClose  { \a s -> PosToken a $ BracC s}
    "||"            { \a s -> PosToken a $ Or }
    "|"             { \a s -> PosToken a $ PatternGuard}
    "!!"            { \a s -> PosToken a $ AccessPort }
    "\"             { \a s -> PosToken a $ Lam }

    @scopedIdentifier {\a s -> PosToken a $ ScopedVar s }
    @identifier     { \a s -> PosToken a $ Var s }

{

clean :: String -> String
clean s = reverse $ rmWs $ reverse $ rmWs s
    where rmWs = dropWhile (\c -> c ==' ' || c == '\t')

traceThis :: (Show a) => a -> a
traceThis a = trace ("DEBUG: " ++ show a) a

data PosToken = PosToken { pos :: AlexPosn
                         , token :: Token }
        deriving (Eq, Show)

data Token
           = Prop
           | Mesh
           | Module
           | Import
           | Where
           | Qualified
           | As
           | Var { fromVar ::String }
           | ScopedVar {fromScopedVar :: String }
           | BracO { fromBracO :: String }
           | BracC { fromBracC :: String }
           | Comma
           | Eq
           | PatternGuard
           | Or
           | Omni
           | Uni
           | DefinedByCol                       -- ':' after definitions
           | Scol                               -- ';' after output spec
           | Indent {fromIndent :: Int}
           | PatternMatchEnd                    -- '->' after PM
           | Negate    
           | Let 
           | LetAssOp                           -- '=' in let x = ...
           | Wire
           | AccessPort
           | Using
           | Lam
           | StringLit {fromStringLit :: String}
           | ExtendedStringLit { exStringLitString :: String
                               , exStringLitVars :: [String] }
           | IntL { fromIntL :: Int }
           | DoubleL { fromDoubleL :: Double }
           | BoolL { fromBoolL :: Bool }
           | PatternMatchImplErr
           | IndentErr
           | EOF 
    deriving (Eq,Show,Data)


lexExtendedString :: String -> Token
lexExtendedString s = ExtendedStringLit s
                        $ getExts s
    where getExts "" = []
          getExts ('%':t) = takeWhile (not . (`elem` illegal)) t : getExts t
          getExts (h:t) = getExts t

          illegal = ['\n', ' ', '\r', '\t', '"']

getTokenOf :: String -> Token
getTokenOf s = fromConstr 
             $ head $ filter ((==s) . map toLower . showConstr) 
             $ dataTypeConstrs $ dataTypeOf $ Prop



}
