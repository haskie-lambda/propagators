module Prop.Language.PRIM where
import Prop.RuntimeTypes (SomeType(..), StrLit(..))

primitives :: [Either String StrLit] -> IO SomeType
primitives [] = error "empty primitive"
primitives (Left "PRIM.print":r) = do 
    print r
    return $ Anything ()
primitives (Left "PRIM.>":Left a:Left b:r) = do
    return $ SBool $ (read a ::Int) > (read b)
primitives [Right (StrLit a)] = return $ SString a
primitives (Left "PRIM.read":_) = error "has to be handled by runtime"
primitives _ = error "unrecognized primitive"
