{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE GADTs #-}
{-|
Module      : InterpreterTypes
Description : Types in use by the interpreter
Copyright   : (c) Fabian Schneider, 2021
License     : MIT
Maintainer  : Fabian Schneider
Stability   : experimental

Types in use by the interpreter, not the language itself;
-}

module Prop.InterpreterTypes where
import Prop.RuntimeTypes
import Data.Map
import Data.UUID
import Control.Monad.STM
import Control.Concurrent.STM.TVar.Lifted
import Control.Concurrent

-- | Type of the Prelude Function
-- The prelude for SIMBA is not a set of functions but one function pattern 
-- matching on the input. This makes the syntax extremely extensible
type PRIM = [Either String StrLit] -> IO SomeType 

-- | The Evnironment of a Module with imports
data ScopeEnvironment = SE { se_imports :: Map String PropSpec
                           , se_currentModule :: PropSpec
                           }

newtype ModuleName = ModuleName { fromModuleName :: String }
    deriving (Ord,Eq,Show)
newtype MeshName = MeshName { fromMeshName :: String }
    deriving (Ord,Eq,Show)
newtype MeshInstanceName = MeshInstanceName { fromMeshInstanceName :: String }
    deriving (Ord,Eq,Show)
newtype StructureName = StructureName { fromStructureName :: String }
    deriving (Ord,Eq,Show)
newtype StructureInstanceName = StructureInstanceName { fromStructureInstanceName :: String }
    deriving (Ord,Eq,Show)
newtype PortSetName = PortSetName { fromPortSetName :: String }
    deriving (Ord,Eq,Show)
newtype PortName = PortName { fromPortName :: String }
    deriving (Ord,Eq,Show)

data MeshQualifiedName 
    = MQN { mqn_moduleName :: ModuleName
          , mqn_meshName :: MeshName
          , mqn_meshInstance :: MeshInstanceName }
    deriving (Ord,Eq,Show)

toStructQualifiedName :: MeshQualifiedName 
                      -> StructureName 
                      -> StructureInstanceName
                      -> StructureQualifiedName
toStructQualifiedName (MQN modN meshN meshI) sn sin
  = SQN modN meshN meshI sn sin


showFQN :: FullyQualifiedName -> String
showFQN fqn = (fromModuleName $ fqn_moduleName fqn) ++ "."
            ++(fromMeshName $ fqn_meshName fqn) ++ "."
            ++(fromMeshInstanceName $ fqn_meshInstance fqn) ++ "."
            ++(fromStructureName $ fqn_structureName fqn) ++ "."
            ++(fromStructureInstanceName $ fqn_structureInstanceName fqn) ++ "."
            ++(fromPortSetName $ fqn_portSetName fqn) ++ "."
            ++(fromPortName $ fqn_portName fqn)

showSQN :: StructureQualifiedName -> String
showSQN sqn = (fromModuleName $ sqn_moduleName sqn) ++ "."
            ++(fromMeshName $ sqn_meshName sqn) ++ "."
            ++(fromMeshInstanceName $ sqn_meshInstance sqn) ++ "."
            ++(fromStructureName $ sqn_structureName sqn) ++ "."
            ++(fromStructureInstanceName $ sqn_structureInstanceName sqn) 

data FullyQualifiedName = FQN { fqn_moduleName :: ModuleName
                              , fqn_meshName :: MeshName
                              , fqn_meshInstance :: MeshInstanceName
                              , fqn_structureName :: StructureName
                              , fqn_structureInstanceName :: StructureInstanceName
                              , fqn_portSetName :: PortSetName
                              , fqn_portName :: PortName }
    deriving (Ord,Eq,Show)

data StructureQualifiedName 
    = SQN { sqn_moduleName :: ModuleName
          , sqn_meshName :: MeshName
          , sqn_meshInstance :: MeshInstanceName
          , sqn_structureName :: StructureName
          , sqn_structureInstanceName :: StructureInstanceName }
    deriving (Ord,Eq,Show)

toQualifiedName :: StructureQualifiedName -> PortSetName -> PortName -> FullyQualifiedName
toQualifiedName sqn psn pn = FQN (sqn_moduleName sqn)
                                 (sqn_meshName sqn)
                                 (sqn_meshInstance sqn)
                                 (sqn_structureName sqn)
                                 (sqn_structureInstanceName sqn)
                                 psn
                                 pn

newtype CellID = CID { ci_uuid :: UUID }
    deriving (Ord,Eq,Show)
newtype TVarID = TID { tvarID :: String }
    deriving (Ord,Eq,Show)

data RuntimeState = RS { rt_threads :: Map StructureQualifiedName ThreadId
                       , rt_port_cell_mapping 
                                    :: Map FullyQualifiedName CellID
                       , rt_cells   :: Map CellID [TVarID]
                       , rt_tvars   :: Map TVarID (TVar (Maybe SomeType))
                       , rt_prim    :: PRIM
                       , rt_inputs  :: Map StructureQualifiedName SomeType
                       }


data ImplementationState = IS { globalState :: TVar RuntimeState
                              , globalSpec :: PropSpec
                              , mesh :: Mesh
                              , mesh_instance_name :: String
                              , propVars :: Map String (String, [Either String ExStrLit])
                              , bindings :: Map String SomeType}

