{
{-# LANGUAGE DeriveDataTypeable #-}
module Lexer where
import Data.Typeable
import Data.Data
import Data.List
import Data.List.Split
import Data.Char
import Debug.Trace
import Prelude hiding (lex)
import Control.Monad (liftM)
}

%wrapper "monadUserState"
$digit            = 0-9
@string           = (. # [\" \\] )

$alpha            = [a-zA-Z]
@real             = ($digit+ \. | $digit * \. $digit +)
@boolLit          = ("True"|"False")
@alphaNum         = ($alpha|$digit)+
$bracketsOpen     = [\(\[\{]
$bracketsClose    = [\)\]\}]
$brackets         = [ $bracketsOpen $bracketsClose]
@identifier       = [^ : ! = \\ \ " $brackets]+
@commaOrSpace     = (\,\ * | \ +)
@scopedIdentifier = @identifier(\.@identifier)+
@globalKeyword    = (prop|mesh|let|omni|uni|let|using|module|import|where)
@port             = (@identifier:\ *)?@identifier
@portSpec         = ((@identifier|@scopedIdentifier):)?
                    " "*
                        \{\ * @port
                            (@commaOrSpace @port)*
                        " "*\} 
@deepPortSpec     = ((@identifier|@scopedIdentifier):)?
                    " "*
                        \{\ * @identifier: (. # \})+ \} 
@indent           = \n[\t\ ]+

tokens :-
    @indent         { \s -> Indent $ length s }
    $white+         ;
    "--".*          ;
    @globalKeyword  { \keyword -> getTokenOf keyword }
    $digit+         { \s -> IntL (read s) }
    @real+          { \s -> DoubleL (read s) }
    @boolLit        { \s -> BoolL (read s) }
    \" @string \"   { \s -> StringLit (tail . init $ s) }
    @portSpec       { \s -> parsePortSpec s } 
    @deepPortSpec   { \s -> parseDeepPortSpec s }
    ":"             { \s -> DefinedByCol }
    ","             { \s -> Comma }
    "!"             { \s -> Negate }
    "=="            { \s -> Eq }
    "="             { \s -> LetAssOp }
    "~>"            { \s -> Wire }
    "->"            { \s -> PatternMatchEnd }
    $bracketsOpen   { \s -> BracO s}
    $bracketsClose  { \s -> BracC s}
    "||"            { \s -> Or }
    "|"             { \s -> PatternGuard}
    "!!"            { \s -> AccessPort }
    '\\'            { \s -> Lam }

    @scopedIdentifier {\s -> ScopedVar s }
    @identifier     { \s -> Var s }
{

clean :: String -> String
clean s = reverse $ rmWs $ reverse $ rmWs s
    where rmWs = dropWhile (\c -> c ==' ' || c == '\t')

parsePortSpecHead :: String -> (Maybe String, String)
parsePortSpecHead s = (name, body)
    where 
        (name', body') = span (/='{') $ clean s
        name = case name' of 
                [] -> Nothing
                a  -> Just $ clean $ init $ clean a
        body = clean $ init . tail $ clean body'

parseDeepPortSpec :: String -> TokenClass
parseDeepPortSpec s = DeepPortSpec name parsedPorts
    where
        (name, body) = parsePortSpecHead s
        ports = "," `splitOn` body
        parsedPorts = map 
                        (\p -> let (l,r) = span (/=':') p
                                   (Right x)= runAlex (tail r) alexMonadScan 
                                in (l, x)) 
                        ports

parsePortSpec :: String -> TokenClass
parsePortSpec s = PortSpec name parsedPorts
    where 
        (name, body) = parsePortSpecHead s
        ports = if ',' `elem` body 
                    then "," `splitOn` (filter (/=' ') body)
                    else ("," `splitOn`) 
                        $ intercalate [] 
                        $ map (\g -> if head g == ' '
                                        then ","
                                        else g) $ group body
        parsedPorts = map (\p -> 
                        if ':' `elem` p 
                            then 
                                let (l,r) = span (/=':') p
                                 in (Just $ clean l, clean $ tail r)
                            else (Nothing, clean p)
                        ) ports

traceThis :: (Show a) => a -> a
traceThis a = trace ("DEBUG: " ++ show a) a

data Token = Token { alexPosn :: AlexPosn, tc :: TokenClass}
    deriving (Eq,Show)

instance Data Token where
    gunfold    = gunfold    . tc
    toConstr   = toConstr   . tc
    dataTypeOf = dataTypeOf . tc

data TokenClass
           = Prop
           | Mesh
           | Module
           | Import
           | Where
           | Var String
           | BracO String
           | BracC String
           | Comma
           | Eq
           | PatternGuard
           | Or
           | ScopedVar String 
           | Omni
           | Uni
           | PortSpec (Maybe String) [(Maybe String, String)]
           | DeepPortSpec (Maybe String) [(String, [Token])]
           | DefinedByCol                       -- ':' after definitions
           | Indent Int
           | PatternMatchEnd                    -- '->' after PM
           | Negate    
           | Let 
           | LetAssOp                           -- '=' in let x = ...
           | Wire
           | AccessPort
           | Using
           | Lam
           | StringLit String
           | IntL Int
           | DoubleL Double
           | BoolL Bool
           | EOF 
    deriving (Eq,Show,Data)

getTokenOf :: String -> Token
getTokenOf s = fromConstr 
             $ head $ filter ((==s) . map toLower . showConstr) 
             $ dataTypeConstrs $ dataTypeOf $ Prop


-- from https://github.com/dagit/happy-plus-alex/blob/master/src/Lexer.x

data AlexUserState = AlexUserState { filePath :: FilePath }

alexInitUserState :: AlexUserState
alexInitUserState = AlexUserState "<unknown>"

getFilePath :: Alex FilePath
getFilePath = liftM filePath alexGetUserState

setFilePath :: FilePath -> Alex ()
setFilePath = alexSetUserState . AlexUserState

unLex :: TokenClass -> String
unLex Prop = "prop"
unLex Mesh = "mesh"
unLex Module = "module"
unLex Import = "import"
unLex Where = "where"
unLex (Var s) = s
unLex (BracO s) = s
unLex (BracC s) = s
unLex Comma = ","
unLex Eq = "=="
unLex PatternGuard = "|"
unLex Or = "||"
unLex (ScopedVar s) = s
unLex Omni = "omni"
unLex Uni = "uni"
unLex (PortSpec ms ls) = show ms <> ": " <> show ls
unLex (DeepPortSpec ms ls) = show ms <> ": " <> show ls
unLex DefinedByCol = ":"   
unLex (Indent i) = "[indent " <> show i <> "]"
unLex PatternMatchEnd = "->"
unLex Negate = "!"
unLex Let = "let"
unLex LetAssOp = "="                      
unLex Wire = "~>"
unLex AccessPort = "!!"
unLex Using = "using"
unLex Lam = "\\"
unLex (StringLit s) = "\"" <> s <> "\""
unLex (IntL i) = show i
unLex (DoubleL d) = show d
unLex (BoolL b) = show b
unLex EOF = "<EOF>"

alexEOF :: Alex Token
alexEOF = do
  (p,_,_,_) <- alexGetInput
  return $ Token p EOF

lex :: (String -> TokenClass) -> AlexAction Token
lex f = \(p,_,_,s) i -> return $ Token p (f (take i s))

lex' :: TokenClass -> AlexAction Token
lex' = lex . const

alexMonadScan' :: Alex Token
alexMonadScan' = do
  inp <- alexGetInput
  sc <- alexGetStartCode
  case alexScan inp sc of
    AlexEOF -> alexEOF
    AlexError (p, _, _, s) ->
        alexError' p ("lexical error at character '" ++ take 1 s ++ "'")
    AlexSkip  inp' len -> do
        alexSetInput inp'
        alexMonadScan'
    AlexToken inp' len action -> do
        alexSetInput inp'
        alexMonadScan'
        -- return $ Token (_ sc) $ action (ignorePendingBytes inp len)

-- Signal an error, including a commonly accepted source code position.
alexError' :: AlexPosn -> String -> Alex a
alexError' (AlexPn _ l c) msg = do
  fp <- getFilePath
  alexError (fp ++ ":" ++ show l ++ ":" ++ show c ++ ": " ++ msg)

-- A variant of runAlex, keeping track of the path of the file we are lexing.
runAlex' :: Alex a -> FilePath -> String -> Either String a
runAlex' a fp input = runAlex input (setFilePath fp >> a)
{-
main = do
    s <- getContents
    mapM_ print (alexScanTokens s)
-}
}
