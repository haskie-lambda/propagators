{-# LANGUAGE GADTs #-}
{-|
Module      : RuntimeTypes
Description : Types in use by the Parser and the Interpreter
Copyright   : (c) Fabian Schneider, 2021
License     : MIT
Maintainer  : Fabian Schneider
Stability   : experimental

Types used in the parser for constructing program representations 
as well as types that are used in the interpreter (for interpreting the 
respresentation) and types for the runtime language itself
-}
module Prop.RuntimeTypes where
import Data.Map
import Prop.Lexer
import Data.Data
import Unsafe.Coerce (unsafeCoerce)


-- | A type for incrementally constructing a program representation 
-- using happy.
-- The Interpreter only uses PropSpec
-- TODO: this should be split up into seperate types that should be an 
-- instance of some class to catch errors more efficiently during development.
data PropSpec = PropSpec { moduleName :: String 
                         , imports :: [Import]
                         , propagators :: Map String Propagator
                         , meshes :: Map String Mesh}
              | PropagatorSpec Propagator
              | PortSetList {fromPortSetList :: [PortSet] }
              | PSPortSet   {fromPSPortSet :: PortSet }
              | PropDefinition { fromPropDefinition :: Either [PatternMatch] [Token] } 
              | PSLambdaVars { fromPSLambdaVars :: [String] }
              | AnyVars { fromAnyVars :: [String] }
              | AnyVar { fromAnyVar :: String }
              | AnyVarsWES { fromAnyVarsWES :: [Either String ExStrLit] }
              | OutputSpecs { fromOutputSpecs :: [OutputSpec] }
              | PSOutputSpec { fromPSOutputSpec :: OutputSpec }
              | MeshSpec { fromMeshSpec :: Mesh }
              | PSMeshSpecification {fromPSMeshSpecification :: MeshSpecification }
              | PSSingleSpec { fromPSSingleSpec :: SingleSpecification }
              | PSWireOp { fromPSWireOp :: WireOp }
              | PSGuards { fromPSGuards :: [Guard] }
              | PSGuard { fromPSGuard :: Guard }
              | DeepPortSpec { fromDeepPortSpec 
                                    :: Map String 
                                           (Either 
                                                (String,String) -- port access 
                                                [Either String ExStrLit] -- cell lambdas
                                           ) }
              | PortBinding { fromPortBinding
                                :: (Either (String, String)
                                   [Either String ExStrLit])}
              | IsUni Bool
              | Unimplemented 
                    deriving (Show, Eq)

newtype StrLit = StrLit { fromStrLit :: String } 

instance Show StrLit where
    show (StrLit a) = show a

data ExStrLit = ExStrLit { exStrLitString :: String
                         , exStrLitVars :: [String] }
            deriving (Show,Eq)

toExStrLit :: PosToken -> ExStrLit
toExStrLit (PosToken _ (ExtendedStringLit a b)) = ExStrLit a b

data Import = ImportDef { importedAs :: Maybe String
                           , fullyQualifiedName :: String }
                    deriving (Show,Eq)
data Propagator = Propagator { prop_name :: String
                             , prop_lambdaVars :: [String]
                             , prop_uniDirectional :: Bool
                             , prop_ports :: [PortSet]
                             , prop_definition :: Either [String]
                                                    (Either [PatternMatch] [Token])
                             , prop_extension :: Maybe String}
                deriving (Show,Eq)

data PortSet = PortSet { portName :: Maybe String
                       , ports :: [String] }
    deriving (Show,Eq)

data PatternMatch = PM { pmListenVar :: [String] 
                       , pmOutputSpecs :: [OutputSpec] }
                  | Guards { fromGuards :: Guard}
                deriving (Show,Eq)

data Guard = Guard { guardListenVarsWithFilters :: [String]
                       , guardFilters :: [[Either String ExStrLit]]
                       , guardOutputSpecs :: [[OutputSpec]] }
            deriving(Show,Eq)

data OutputSpec = OS { osNames :: [String]
                     , osExpr  :: [Either String ExStrLit] }
    deriving (Show,Eq)

data Mesh = Mesh { mesh_name :: String
                 , mesh_lambdaVars :: [String]
                 , mesh_uniDirectional :: Bool
                 , mesh_ports :: [PortSet]
                 , mesh_definition :: MeshSpecification 
                 , mesh_extension :: Maybe String } 
    deriving (Show,Eq)

type MeshSpecification = [SingleSpecification]

data SingleSpecification = LetExpr  { let_var :: String
                                    , let_expr :: [Either String ExStrLit] }
                         | BindExpr { bind_var :: String
                                    , bound_struct :: String 
                                    , bound_ports :: [PortSet] }
                         | WireExpr { wire_ops :: [WireOp] }
    deriving (Show, Eq) 

data WireOp = TrivialOp {fromTrivialOp :: String}
            | TrivialExpr {fromTrivialExpr::[String]}
            | PortAccess { portAccessStruct :: String 
                         , accessedPort :: String }
            | DeepPortSpecification 
                        { fromDeepPortSpecification
                                :: Map String 
                                       (Either 
                                            (String,String) -- port access 
                                            [Either String ExStrLit] -- cell lambdas
                                       ) }
    deriving (Show, Eq)




data SomeType = SBool Bool
              | SInt Int
              | SDouble Double
              | SString String
              | SMaybe (Maybe SomeType)
              | STuple (SomeType, SomeType)
              | forall a. (Data a, Eq a, Show a) => Anything a

printST (SBool b) = show b
printST (SInt b) = show b
printST (SDouble b) = show b
printST (SString b) = show b
printST (SMaybe b) = show b
printST (STuple (a,b)) = show (a,b)
printST (Anything a) = show a

instance Show SomeType where
    show (SBool b) = "SBool " ++ show b
    show (SInt b) = "SInt " ++ show b
    show (SDouble b) = "SDouble " ++ show b
    show (SString b) = "SString " ++ show b
    show (SMaybe b) = "SMaybe " ++ show b
    show (STuple (a,b)) = "STuple (" ++ show a ++ ", " ++ show b ++ ")"
    show (Anything a) = "Anything " ++ show a

instance Eq SomeType where
    (SBool a) == (SBool b) = a == b
    (SInt a) == (SInt b) = a == b
    (SDouble a) == (SDouble b) = a == b
    (SString a) == (SString b) = a == b
    (SMaybe a) == (SMaybe b) = a == b
    (STuple a) == (STuple b) = a == b
    (Anything a) == (Anything b) = 
        (dataTypeRep $ dataTypeOf a) == (dataTypeRep $ dataTypeOf b)
        && toConstr a == toConstr b 
        && a == (unsafeCoerce b)


