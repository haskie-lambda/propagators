{
--{-# OPTIONS -w #-}

{-|
Module      : Parser
Description : A Happy parser for SIMBA
Copyright   : (c) Fabian Schneider, 2021
License     : MIT
Maintainer  : Fabian Schneider
Stability   : experimental

A working, complete yet inefficient and insecure parser using happy to parse SIMBA
programs.
TODO: change this after changing the RuntimeTypes
TODO: add some comments to the invariants and non obvious functions
-}
module Prop.Parser where
import Prop.Lexer as Lexer
import Data.Map
import Data.List (group, sort)
import Data.Maybe (catMaybes)
import Control.Monad.Trans.State.Strict
import Control.Monad
import System.IO.Unsafe
import System.Exit
import Debug.Trace
import Prop.RuntimeTypes as RuntimeTypes

}

%name parse
%tokentype { PosToken }
%error { happyErrorG }
%monad { State ParserState }

%token 
      Prop                  { PosToken _ Prop          }
      Mesh                  { PosToken _ Lexer.Mesh    }
      Module                { PosToken _ Module        }
      Import                { PosToken _ Import        }
      Where                 { PosToken _ Where         }
      Qualified             { PosToken _ Qualified     }
      As                    { PosToken _ As            }
      Var                   { PosToken _ (Var _)         }
      ScopedVar             { PosToken _ (ScopedVar _)   }
      BracO                 { PosToken _ (BracO _)       }
      BracC                 { PosToken _ (BracC _)       }
      Comma                 { PosToken _ Comma         }
      Eq                    { PosToken _ Eq            }
      PatternGuard          { PosToken _ PatternGuard  }
      Or                    { PosToken _ Or            }
      Omni                  { PosToken _ Omni          }
      Uni                   { PosToken _ Uni           }
      DefinedByCol          { PosToken _ DefinedByCol  }
      Scol                  { PosToken _ Scol          }
      Indent                { PosToken _ (Indent _)      }
      PatternMatchEnd       { PosToken _ PatternMatchEnd }
      Negate                { PosToken _ Negate        }
      Let                   { PosToken _ Let           }
      LetAssOp              { PosToken _ LetAssOp      }
      Wire                  { PosToken _ Wire          }
      AccessPort            { PosToken _ AccessPort    }
      Using                 { PosToken _ Using         }
      Lam                   { PosToken _ Lam           }
      StringLit             { PosToken _ (StringLit _)   }
      ExtendedStringLit     { PosToken _ (ExtendedStringLit _ _) }
      IntL                  { PosToken _ (IntL _)        }
      DoubleL               { PosToken _ (DoubleL _)     }
      BoolL                 { PosToken _ (BoolL _)       }
      EOF                   { PosToken _ EOF           }

      

%%

Program : Module ModuleName Where 
          Imports  
          Definitions               { addModuleName (fromAnyVar $2) (addImports $4 $5) } 
Imports : Import Var Imports        { addImport $2 $3 }
        | Import ScopedVar Imports  { addImport $2 $3 }
        | Import Qualified Var 
            As Var Imports          { addQImport $3 $5 $6 }
        | Import Qualified ScopedVar
            As Var Imports          { addQImport $3 $5 $6 }
        |                           { PropSpec "" [] (fromList []) (fromList []) }

Definitions : Prop PropDefinition Definitions
                { addProp $2 $3 }
            | Mesh MeshDefinition Definitions
                { addMesh $2 $3 }
            |   { PropSpec "" [] (fromList []) (fromList []) }

ModuleName : Var {% do
                    modify (\s -> s { currentModule = (fromVar $ token $1) })
                    return $ AnyVar (fromVar $ token $1)
                }
PropName : Var {% do
                    modify (\s -> s { currentDefinition = (fromVar $ token $1) })
                    return $ AnyVar (fromVar $ token $1)
                }
                    
MeshName : Var {% do
                    modify (\s -> s { currentDefinition = (fromVar $ token $1) })
                    return $ AnyVar (fromVar $ token $1)
                }

PropDefinition : PropName UniOrOmni PortSpecifications Using Var MLambda PatternMatchEnd
                    PropImplementation
                    {% do
                        let p = Propagator
                                    (fromAnyVar $1)
                                    (fromPSLambdaVars $6)
                                    ($2 == IsUni True)
                                    (fromPortSetList $3)
                                    (Right $ fromPropDefinition $8)
                                    (Just $ fromVar $ token $5)
                        validatePropagator (pos $4) p
                    }
               | PropName UniOrOmni PortSpecifications DefinedByCol
                    Indent PropImplementation
                    {% do 
                        (PS _ n ind matchVars) <- get
                        when ((fromIndent $ token $5)/= ind) $
                            (happyError (pos $5) [token $5,Var n
                                                   , IndentErr])
                        let p = Propagator
                                    (fromAnyVar $1)
                                    [] -- lambda vars
                                    ($2 == IsUni True)
                                    (fromPortSetList $3)
                                    (Right $ fromPropDefinition $6)
                                    Nothing 
                        validatePropagator (pos $4) p
                    }
               | PropName UniOrOmni PortSpecifications MLambda PatternMatchEnd
                    Indent PropImplementation
                    {% do 
                        (PS _ n ind matchVars) <- get
                        when ((fromIndent $ token $6)/= ind) $
                            (happyError (pos $6) [token $6,Var n
                                                   , IndentErr])
                        let p = Propagator
                                    (fromAnyVar $1)
                                    (fromPSLambdaVars $4)
                                    ($2 == IsUni True)
                                    (fromPortSetList $3)
                                    (Right $ fromPropDefinition $7)
                                    Nothing 
                        validatePropagator (pos $5) p
                    }
               | PropName UniOrOmni PortSpecifications Using Var DefinedByCol
                    AnyTokens
                    {% do
                        let p = Propagator
                                    (fromAnyVar $1)
                                    [] -- lambda vars
                                    ($2 == IsUni True)
                                    (fromPortSetList $3)
                                    (Left $ fromAnyVars $7)
                                    (Just $ fromVar $ token $5)
                        validatePropagator (pos $4) p
                    }


Guard : MixedLits PatternMatchEnd 
            MaybeIndent OutputSpecs Scol Indent PatternGuard Guard
            {% do 
                (PS _ n ind matchVars) <- get
                when ((fromIndent $ token $6)<=ind) $
                    (happyError (pos $6) [token $6,Indent ind, IndentErr])
                
                let (Guard _ prevFilters prevOutputSpecs) = (fromPSGuard $8)
                return $ PSGuard $
                        (Guard (matchVars)
                               ((fromAnyVarsWES $1) : prevFilters )
                               ((fromOutputSpecs $4) : prevOutputSpecs) 
                                )
                                }
      | MixedLits PatternMatchEnd 
            MaybeIndent OutputSpecs Scol
            {% do 
                (PS _ n ind matchVars) <- get
                
                return $ PSGuard $
                        (Guard (matchVars)
                               [fromAnyVarsWES $1]
                               [fromOutputSpecs $4]
                        )
                                }

MatchVars : MixedVars {% do 
                            modify (\s -> s { currentPMVars = fromAnyVars $1 })
                            return $1 }

PropImplementation : Var MatchVars 
                        Indent PatternGuard Guard Indent PropImplementation
                        {% do 
                            (PS _ n ind _) <- get
                            when ((fromVar $ token $1)/=n) $ 
                                (happyError (pos $1) [token $1,Var n
                                                   , PatternMatchImplErr]) 
                            when ((fromIndent $ token $6)/= ind) $
                                (happyError (pos $6) [token $6,Var n
                                                   , IndentErr])
                            when ((fromIndent $ token $3)<=ind) $
                                (happyError (pos $3) [token $3,Var n
                                                   , IndentErr])
                            return $ PropDefinition $ Left $
                                (Guards $ fromPSGuard $5)
                                   : (fromLeft $ fromPropDefinition $7) }
                   | Var MatchVars
                        Indent PatternGuard Guard
                        {% do 
                            (PS _ n ind _) <- get
                            when ((fromVar $ token $1)/=n) $ 
                                (happyError (pos $1) [token $1,Var n
                                                   , PatternMatchImplErr]) 
                            when ((fromIndent $ token $3)<=ind) $
                                (happyError (pos $3) [token $3,Var n
                                                   , IndentErr])
                            return $ PropDefinition $ Left $
                                [(Guards $ fromPSGuard $5)]
                                    }
                   | Var MixedVars PatternMatchEnd
                            Indent OutputSpecs Scol Indent PropImplementation
                        {% do 
                            return $ trace "here" ()
                            (PS _ n ind _) <- get
                            when ((fromVar $ token $1)/=n) $ 
                                (happyError (pos $1) [token $1,Var n
                                                   , PatternMatchImplErr]) 
                            when ((fromIndent $ token $7)/= ind) $
                                (happyError (pos $7) [token $7,Var n
                                                   , IndentErr])
                            when ((fromIndent $ token $4)<=(ind +1)) $
                                (happyError (pos $4) [token $4,Var n
                                                   , IndentErr])
                            return $ PropDefinition $ Left $
                                (PM (fromAnyVars $2) 
                                    (fromOutputSpecs $5))
                                    : (fromLeft $ fromPropDefinition $8)}
                   | Var MixedVars PatternMatchEnd
                            Indent OutputSpecs Scol
                        {% do 
                            return $ trace "here2" ()
                            (PS _ n ind _) <- get
                            when ((fromVar $ token $1)/=n) $ 
                                (happyError (pos $1) [token $1,Var n
                                                   , PatternMatchImplErr]) 
                            --when ((fromIndent $ token $1)/= ind) $
                            --    (happyError (pos $1) [token $1,Var n
                            --                       , IndentErr])
                            when ((fromIndent $ token $4)<=(ind +1)) $
                                (happyError (pos $4) [token $4,Var n
                                           , IndentErr])
                            return $ PropDefinition $ Left $
                                [(PM (fromAnyVars $2) 
                                    (fromOutputSpecs $5))]
                                    }

MaybeIndent : Indent {Unimplemented}
            |  {Unimplemented}

OutputSpecs : MixedVars DefinedByCol
                MaybeIndent MixedVarsWExStringLit Scol
              Indent OutputSpecs 
                {% do 
                    (PS _ n ind _) <- get
                    when ((fromIndent $ token $2)<=(ind +1)) $
                        (happyError (pos $2) [token $2,Var n
                                           , IndentErr])
                    return $ OutputSpecs $ 
                        (OS (fromAnyVars $1)
                            (fromAnyVarsWES $4))
                        : (fromOutputSpecs $7) }
            | MixedVars DefinedByCol
                MaybeIndent MixedVarsWExStringLit Scol
                    { OutputSpecs [OS (fromAnyVars $1)
                                      (fromAnyVarsWES $4)]}

UniOrOmni : Uni         { IsUni True  }
          | Omni        { IsUni False }

MLambda :               { PSLambdaVars []               }
        | Lam Vars      { PSLambdaVars (fromAnyVars $2) }


AnyVar : Var { AnyVar (fromVar $ token $1) }
       | ScopedVar { AnyVar (fromScopedVar $ token $1) }



MixedLits  
     : Var MixedLits { AnyVarsWES $ (Left $ fromVar $ token $1):(fromAnyVarsWES $2)       }
     | ScopedVar MixedLits   { AnyVarsWES $ (Left $ fromScopedVar $ token $1):(fromAnyVarsWES $2) }
     | ExtendedStringLit MixedLits 
                { AnyVarsWES 
                   $ (Right $ toExStrLit $1)
                        :(fromAnyVarsWES $2) }
     | IntL MixedLits
       { AnyVarsWES $ (Left $ show $ fromIntL $ token $1) : (fromAnyVarsWES $2) }
     | IntL 
       { AnyVarsWES $ [Left $ show $ fromIntL $ token $1]}
     | Var              { AnyVarsWES [Left $ fromVar $ token $1] }
     | ScopedVar        { AnyVarsWES [Left $ fromScopedVar $ token $1]      }
     | ExtendedStringLit 
                { AnyVarsWES [Right $ toExStrLit $1] }



MixedVarsWExStringLit 
     : Var MixedVarsWExStringLit    { AnyVarsWES $ (Left $ fromVar $ token $1):(fromAnyVarsWES $2)       }
     | ScopedVar MixedVarsWExStringLit   { AnyVarsWES $ (Left $ fromScopedVar $ token $1):(fromAnyVarsWES $2) }
     | ExtendedStringLit MixedVarsWExStringLit 
                { AnyVarsWES 
                   $ (Right $ toExStrLit $1)
                        :(fromAnyVarsWES $2) }
     | Var              { AnyVarsWES [Left $ fromVar $ token $1]                          }
     | ScopedVar        { AnyVarsWES [Left $ fromScopedVar $ token $1]                    }
     | ExtendedStringLit 
                { AnyVarsWES [Right $ toExStrLit $1] }

MixedVars : Var MixedVars    { AnyVars $ (fromVar $ token $1):(fromAnyVars $2)       }
     | ScopedVar MixedVars   { AnyVars $ (fromScopedVar $ token $1):(fromAnyVars $2) }
     | Var              { AnyVars [fromVar $ token $1]                          }
     | ScopedVar        { AnyVars [fromScopedVar $ token $1]                    }


MixedExprsMin2 : MixedExpr MixedExprs { AnyVars $ (fromAnyVar $1):(fromAnyVars $2) }

MixedExprs : MixedExpr MixedExprs { AnyVars $ (fromAnyVar $1):(fromAnyVars $2) }
           | MixedExpr { AnyVars [fromAnyVar $1] }

MixedExpr  : Var       { AnyVar $ fromVar $ token $1 }
           | ScopedVar { AnyVar $ fromScopedVar $ token $1 }
           | IntL      { AnyVar $ show $ fromIntL $ token $1 }
           | DoubleL   { AnyVar $ show $ fromDoubleL $ token $1 }
           | BoolL     { AnyVar $ show $ fromBoolL $ token $1 }

Vars : Var Vars         { AnyVars $ (fromVar $ token $1):(fromAnyVars $2)       }
     | Var              { AnyVars [fromVar $ token $1]                          }

AnyTokens : AnyToken AnyTokens { AnyVars $ (fromAnyVar $1):(fromAnyVars $2) }
          | AnyToken           { AnyVars [fromAnyVar $1]                 }

AnyToken : Module           { AnyVar "module" } 
         | Import           { AnyVar "import" }
         | Indent           { AnyVar "" }
         | Where            { AnyVar "where"  }
         | Qualified        { AnyVar "qualified" }
         | As               { AnyVar "as" }
         | Var              { AnyVar (fromVar $ token $1) }
         | ScopedVar        { AnyVar (fromScopedVar $ token $1) }
         | BracO            { AnyVar (fromBracO $ token $1) }
         | BracC            { AnyVar (fromBracC $ token $1) }
         | Comma            { AnyVar "," }
         | Eq               { AnyVar "==" }
         | PatternGuard     { AnyVar "|" }
         | Or               { AnyVar "||" }
         | Omni             { AnyVar "omni" }
         | Uni              { AnyVar "uni" }
         | DefinedByCol     { AnyVar ":"   }
         | PatternMatchEnd  { AnyVar "->"  }
         | Negate           { AnyVar "!"   }
         | Let              { AnyVar "let" }
         | LetAssOp         { AnyVar "="   }
         | Wire             { AnyVar "~>"  }
         | AccessPort       { AnyVar "!!"  }
         | Using            { AnyVar "using" }
         | Lam              { AnyVar "\\"    }
         | StringLit        { AnyVar $ fromStringLit $ token $1 }
         | ExtendedStringLit{ AnyVar $ exStringLitString $ token $1 }
         | IntL             { AnyVar $ show $ fromIntL $ token $1 } 
         | DoubleL          { AnyVar $ show $ fromDoubleL $ token $1 }
         | BoolL            { AnyVar $ show $ fromBoolL $ token $1 }

PortSpecifications : PortSpecification PortSpecifications
                        { PortSetList $ (fromPSPortSet $1) 
                                        : (fromPortSetList $2) }
                   | PortSpecification  { PortSetList [fromPSPortSet $1] }

PortSpecification : Var DefinedByCol BracO Vars BracC
                        {% do
                            when (token $3 /= BracO "{") $
                                    (happyError (pos $3) [token $3, BracO "{"])
                            when (token $5 /= BracC "}") $
                                    (happyError (pos $5) [token $5, BracC "}"])
                            return $ PSPortSet 
                                   $ PortSet (Just $ fromVar $ token $1)
                                             (fromAnyVars $4)
                        }
                  | BracO Vars BracC
                        {% do
                            when (token $1 /= BracO "{") $
                                    (happyError (pos $1) [token $1, BracO "{"])
                            when (token $3 /= BracC "}") $
                                    (happyError (pos $3) [token $3, BracC "}"])
                            return $ PSPortSet 
                                   $ PortSet Nothing
                                             (fromAnyVars $2)
                        }



MeshDefinition :  MeshName UniOrOmni PortSpecifications MLambda Using Var DefinedByCol                    MeshImplementation
                    { MeshSpec 
                            $ validateMesh 
                            $ RuntimeTypes.Mesh
                                (fromAnyVar $1)
                                (fromPSLambdaVars $4)
                                ($2 == IsUni True)
                                (fromPortSetList $3)
                                (fromPSMeshSpecification $8)
                                (Just $ fromVar $ token $6) }
               |  MeshName UniOrOmni PortSpecifications MLambda DefinedByCol
                    MeshImplementation
                    { MeshSpec 
                            $ validateMesh 
                            $ RuntimeTypes.Mesh
                                (fromAnyVar $1)
                                (fromPSLambdaVars $4)
                                ($2 == IsUni True)
                                (fromPortSetList $3)
                                (fromPSMeshSpecification $6)
                                Nothing }
               |  MeshName DefinedByCol 
                    MeshImplementation
                    { MeshSpec 
                            $ validateMesh 
                            $ RuntimeTypes.Mesh
                                (fromAnyVar $1)
                                [] -- vars
                                (True) --uni
                                [] -- ports
                                (fromPSMeshSpecification $3)
                                Nothing }

MeshImplementation : Indent Specification SpecificationLines
                        {% do 
                            modify (\s -> s { currentIndent = (fromIndent $ token $1) })
                            return $ PSMeshSpecification
                                   $ ((fromPSSingleSpec $2) 
                                        : (fromPSMeshSpecification $3)) }

SpecificationLines : Indent Specification SpecificationLines
                        {% do
                            (PS _ _ ind _) <- get 
                            when ((fromIndent $ token $1) /= ind) $
                                (happyError (pos $1) [token $1, Indent ind]) 
                            return 
                                $ PSMeshSpecification $ (fromPSSingleSpec $2) 
                                              : (fromPSMeshSpecification $3) }
                   |    { PSMeshSpecification $ [] }

Specification : Let Var LetAssOp MixedLits
                        { PSSingleSpec $
                            LetExpr (fromVar $ token $2)
                                  (fromAnyVarsWES $4)
                        }
              | Let Var LetAssOp Var PortSpecifications
                        { PSSingleSpec $
                            BindExpr (fromVar $ token $2)
                                     (fromVar $ token $4)
                                     (fromPortSetList $5)
                        }
              | WireExpression
                        { $1 }

WireExpression : WireOp Wire WireExpression
                        { PSSingleSpec
                            $ WireExpr 
                            $ (fromPSWireOp $1) : 
                                (wire_ops $ fromPSSingleSpec $3) }
               | WireOp Wire WireOp 
                        { PSSingleSpec $ 
                            WireExpr [fromPSWireOp $1
                                     ,fromPSWireOp $3] }

WireOp : AnyVar            -- for simplistic bindings like ` ~> out`
                        { PSWireOp $ TrivialOp (fromAnyVar $1) }
       | MixedExprsMin2 { PSWireOp $ TrivialExpr (fromAnyVars $1) }

       | AnyVar AccessPort AnyVar -- for simple port access like `s1!!d ~>`
                        { PSWireOp $ PortAccess (fromAnyVar $1) (fromAnyVar $3) }
       | BracO PortBindings BracC 
                {% do
                    when ((fromBracO $ token $1) /= "{") $
                        (happyError (pos $1) [token $1, BracO "{"]) 
                    when ((fromBracC $ token $3) /= "}") $
                        (happyError (pos $3) [token $3, BracC "}"]) 
                    return $ PSWireOp 
                           $ DeepPortSpecification 
                           $ fromDeepPortSpec $2
                }

PortBindings : AnyVar DefinedByCol PortBinding Comma
                    PortBindings
                { DeepPortSpec $ 
                    insert (fromAnyVar $1) 
                           (fromPortBinding $3)
                           (fromDeepPortSpec $5) }
             | AnyVar DefinedByCol PortBinding
                { DeepPortSpec $
                    fromList [(fromAnyVar $1
                              , fromPortBinding $3)] }

PortBinding : AnyVar AccessPort Var 
                { PortBinding $ Left (fromAnyVar $1, fromVar $ token $3) }
            | BracO MixedLits BracC
                { PortBinding $ Right (fromAnyVarsWES $2) }



{

-------------------------------------------------------------------------------
-------------------  Types, Operations and Definitions ------------------------
-------------------------------------------------------------------------------

data ParserState = PS { currentModule :: String
                      , currentDefinition :: String
                      , currentIndent :: Int
                      , currentPMVars :: [String] }

    deriving Show


initialState = PS "<unknown>" "<unknown>" 0 []

validatePropagator :: Monad m => AlexPosn -> Propagator -> StateT ParserState m PropSpec
validatePropagator pos prop
    | any (`elem` (catMaybes $ Prelude.map portName $ prop_ports prop)) 
          (prop_lambdaVars prop)
          = happyError pos [Var $ "PortSpecification names may not occur "
                           ++     "as lambda variables"]
    | any (containsDuplicates) $ prop_ports prop
          = happyError pos [Var $ "Port names may not appear multiple times "
                           ++     "in a PortSpecification."]
    | otherwise = return $ PropagatorSpec prop

containsDuplicates :: PortSet -> Bool
containsDuplicates = any (>1)
                   . Prelude.map length
                   . group 
                   . sort
                   . ports 

validateMesh :: Mesh -> Mesh
validateMesh a = a

addModuleName :: String -> PropSpec -> PropSpec
addModuleName a prop = prop { moduleName = a }

addProp :: PropSpec -> PropSpec -> PropSpec
addProp (PropagatorSpec prop) spec 
        = spec { propagators = insert (prop_name prop) prop (propagators spec) }

addMesh:: PropSpec -> PropSpec -> PropSpec
addMesh (MeshSpec mesh) spec 
        = spec { meshes = insert (mesh_name mesh) mesh (meshes spec) }

addImports :: PropSpec -> PropSpec -> PropSpec
addImports (PropSpec _ imports _ _) (PropSpec name _ props meshes) 
        = PropSpec name imports props meshes

addImport :: PosToken -> PropSpec -> PropSpec
addImport (PosToken _ (Var s)) spec = spec { imports = (ImportDef Nothing $ "module." ++ s):(imports spec) }
addImport (PosToken _ (ScopedVar s)) spec = spec { imports = (ImportDef Nothing s):(imports spec) }

addQImport :: PosToken -> PosToken -> PropSpec -> PropSpec
addQImport (PosToken _ (Var s)) (PosToken _ (Var asName)) spec = spec { imports = (ImportDef (Just asName) $ "module." ++ s):(imports spec) }
addQImport (PosToken _ (ScopedVar s)) (PosToken _ (Var asName)) spec = spec { imports = (ImportDef (Just asName) s):(imports spec) }



exitError mod el (AlexPn _ l c) s = unsafePerformIO $ do
                putStrLn $ "Parse-Error:\n"
                         ++"In module '" ++ mod ++ "' "
                         ++"in element '" ++ el ++ "':\n"
                         ++ s ++ "\n  at: " 
                            ++ mod ++ ":" ++ (show l) ++ ":" ++ (show c)
                exitWith $ ExitFailure 1

happyErrorG [] = happyError (AlexPn 0 0 0) []
happyErrorG ts@(h:_) = happyError (pos h) $ Prelude.map token ts

happyError :: Monad m => AlexPosn -> [Token] -> StateT ParserState m a
happyError pos [Var custom] = do
                        (PS mod def ind _) <- get
                        return $ exitError mod def pos
                            $ custom
happyError pos [As, (Var v)] = do
                        (PS mod def ind _) <- get
                        return $ exitError mod def pos
                            $ "missing 'qualified' in qualified import "
                            ++"in import for '" ++ v ++ "'"
happyError pos [Var a, Var b, PatternMatchImplErr]
                        = do
                        (PS mod def ind _) <- get
                        return $ exitError mod def pos
                            $ "Pattern Match Error:\n" 
                            ++"equation for '" ++ a ++ "' in wrong "
                            ++"propagator '" ++ b ++"'"
happyError pos [Indent i,IndentErr] 
                        = do
                        (PS mod def ind _) <- get
                        return $ exitError mod def pos
                            $ "Indent Error:\n"
                            ++"indent too small"
happyError pos [Indent i, Indent j] 
                        = do
                        (PS mod def ind _) <- get
                        return $ exitError mod def pos
                            $ "Indent Error:\n"
                            ++"indent is " ++ (show i) ++ "\n"
                            ++"while it should be " ++ (show j)
happyError pos [BracO a,BracO b]
                        = do
                        (PS mod def ind _) <- get
                        return $ exitError mod def pos
                            $ "Bracketing Error:\n"
                            ++"expected '" ++ b ++ "' but got '" ++ a ++ "'"
                            ++"in port specification"
happyError pos [BracC a,BracC b]
                        = do
                        (PS mod def ind _) <- get
                        return $ exitError mod def pos
                            $ "Bracketing Error:\n"
                            ++"expected '" ++ b ++ "' but got '" ++ a ++ "'"
                            ++"in port specification"
happyError pos (Prop : Var _ : _)
                        = do
                        (PS mod def ind _) <- get
                        return $ exitError mod def pos
                            $ "Did you forget a ';' in the last definition?\n"
                            ++"expected ';;' but got too few"
                            
happyError pos t 
                        = do
                        (PS mod def ind _) <- get
                        return $ exitError mod def pos
                            $ ("parse error at token '" ++ (show $ Prelude.take 8 t) ++ "'")



fromLeft (Left a) = a
}

