{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE TypeApplications #-}
{-|
Module      : Language
Description : Implementation of the SIMBA language semantics
Copyright   : (c) Fabian Schneider, 2021
License     : MIT
Maintainer  : Fabian Schneider
Stability   : experimental

This module contains the complete definition of program semantics for
the SIMBA language.
-}
module Prop.Language where
import Prop.RuntimeTypes -- for Type representations
import Prop.InterpreterTypes
import Prop.Parser       -- for runtime manipulation of datastructure
import System.Exit
import System.IO
import qualified Data.Map as M
import Data.Map ((!?), (!), Map, fromList, member, insert, assocs)
import Control.Monad
import Text.Pretty.Simple (pPrint)
import Data.Maybe (isNothing, isJust, fromMaybe, catMaybes)
import Data.Either (isLeft,isRight)
import Control.Monad.STM
import Control.Concurrent
import Data.UUID.V4 as UUID
import Data.UUID 
import Control.Concurrent.STM.TVar.Lifted
import Data.List (intercalate)
import Data.List.Split (splitOn)
import Data.Foldable (foldrM)
import Control.Monad.Trans.State.Strict
import Control.Monad.Trans.Class
import Control.Monad.Trans.Maybe
import Control.Concurrent.MVar
import Control.Error.Util (hoistMaybe)

-- | given 
-- - a prelude module (a prelude function) 
-- - an import list
-- - and a module specification 
-- builds the runtime representation of the program and starts it
-- This is a non-blocking call ending, however, in an IO repl with basic functionality
-- to modify memory contents of the system.
-- when `exit` is entered into the repl, the system will shutdown and 
-- the repl (and blocking call) will end.
run :: PRIM -> Map String PropSpec -> PropSpec -> IO ()
run prim inScope prop@(PropSpec _ _ _ _) = do
    --pPrint prop
    --putStrLn "------------"
    (finished, globalState) <- buildRuntime (SE inScope prop) prim
    
    waitUntil finished
    putStrLn "Shutting down"
 where
     waitUntil mvar = do
         res <- tryReadMVar mvar
         case res of
           Nothing -> yield >> threadDelay 2000 >> waitUntil mvar
           _ -> return ()
-- needed because of bad implementation of the RuntimeType for program Representations
run _ _ prop = do
    putStrLn $ "Interpreter Error\n"
            ++ "Module '" ++ (moduleName prop) ++ "' was unparsable.\n"
            ++ "Only parts of the module were parsed, "
            ++ "this is probably an interpreter error"
    exitWith $ ExitFailure 2


-- | retrieves the list of variables a specific cell in a specific runtime instance
-- points to by cell id
-- conceptually one level of abstraction below 'resolve'
resolveCell :: TVar RuntimeState -> CellID -> IO [TVar (Maybe SomeType)]
resolveCell t cid = do
    (RS _ p c t _ _) <- readTVarIO t
    return $ (map (t !) . (c !)) cid

-- | retrieves the list of variables a specific cell in a specific runtime instance
-- points to by the qualified name of the cell
resolve :: TVar RuntimeState -> FullyQualifiedName -> IO [TVar (Maybe SomeType)]
resolve t fqn = do 
    (RS _ p c t _ _) <- readTVarIO t
    return $ map (t !) (c ! (p ! fqn))

-- | provides an empty runtime state without any variable, value or cell bindings.
emptyRuntimeState :: PRIM -> RuntimeState
emptyRuntimeState prim = RS (fromList []) (fromList []) 
                            (fromList []) (fromList [])
                            prim (fromList [])

-- | builds the runtime representation of a module
-- NOTE: imports are ignored for now
buildRuntime :: ScopeEnvironment -> PRIM -> IO (MVar (), TVar RuntimeState)
buildRuntime (SE _ curr) prim = do
    -- initialize the state
    globalState <- newTVarIO $ emptyRuntimeState prim
    
    -- disallowing imports leads to the necessity of the module needing a main mesh 
    when (not $ "main" `member` meshes curr) $ do
        putStrLn $ "Module '" ++ (moduleName curr) ++ "' " ++
                   "needs a 'main' mesh but none was found."
        exitWith $ ExitFailure 4
    
    -- build the main mesh
    -- TODO: build the other meshes in the current module
    --       qualified naming already allows for it; implementation should be quite easy
    let main = ((meshes curr) ! "main"):: Mesh
        mqnMain = MQN (ModuleName $ moduleName curr)
                      (MeshName "main")
                      (MeshInstanceName "main")
                      
    -- building the runtime representation of the specification for the mesh
    createThreadsFor globalState mqnMain curr main
   
    -- create the possibility to shut the system down by using the repl
    finished <- newEmptyMVar :: IO (MVar ())
    putStrLn $ "finished building runtime"
    
    -- binding the repl to the shutdown functionality 
    threadId <- createInteractionThread globalState finished

    return (finished, globalState)

-- | the repl for the system.
-- once the systems basic components are started and the rest of the system 
-- is bootstrapped, the repl starts.
-- It provides means to 
-- - exit the system
-- - print the connections of the varibles by showing their 
--   connection UUIDs
-- - print the contents of variables
-- - showing the complete global state
-- - setting variables to specific values
createInteractionThread :: TVar RuntimeState -> MVar () -> IO ThreadId
createInteractionThread gs finished = do
    putStrLn "interaction thread started"
    tid <- forkIO $ interact " "
    return $ tid 

    where   
        interact !pre = do
            -- experimental value that works well with flushing
            threadDelay 5000
            (RS _ port_cell cell_id id_var _ _) <- readTVarIO gs
            putStr $ pre ++ "> "
            hFlush stdout
            line <- splitOn " " <$> getLine
            case line of
              [] ->   interact " "
              [""] -> interact " "
              ("exit":_) -> putMVar finished ()
              ("show":"vars":_) -> do
                  (RS threads port_cell cell_id id_var prim inputs) <- readTVarIO gs

                  putStrLn "-------------------------"
                  putStrLn "id_var"
                  mapM_ (\(tid,var) -> do
                            val <- readTVarIO var 
                            pPrint (tid, printST <$> val)) $ assocs id_var
                  putStrLn "-------------------------"
                  interact " "

              ("show":"state":_) -> do
                  (RS threads port_cell cell_id id_var prim inputs) <- readTVarIO gs
                  putStrLn "-------------------------"
                  putStrLn "threads"
                  pPrint threads
                  putStrLn "port_cell"
                  pPrint port_cell
                  putStrLn "cell_id"
                  pPrint cell_id
                  putStrLn "id_var"
                  mapM_ (\(tid,var) -> do
                            val <- readTVarIO var 
                            pPrint (tid, printST <$> val)) $ assocs id_var
                  putStrLn "inputs"
                  pPrint inputs
                  putStrLn "-------------------------"
                  interact " "
              ("get":var:[]) -> do
                  let splits = splitOn "." var
                      portSetName = if length splits > 6
                                       then splits !! 5
                                       else ""
                      portName = if length splits > 6
                                    then splits !! 6
                                    else splits !! 5
                      sqn = SQN (ModuleName $ splits!!0)
                                (MeshName $ splits!!1)
                                (MeshInstanceName $ splits!!2)
                                (StructureName $ splits !! 3)
                                (StructureInstanceName $ splits !! 4)
                      fqn = toQualifiedName sqn
                                (PortSetName portSetName)
                                (PortName portName)
                      cellId = (port_cell)!? fqn
                      mcells :: Maybe [TVarID]
                      mcells = ((cell_id)!?) =<< cellId
                      vars :: Maybe [TVar (Maybe SomeType)]
                      vars = mcells >>= (\cells ->
                                Just $ catMaybes $ map (id_var!?) cells) 
                  
                  case vars of
                    Nothing -> putStrLn "cell not found" 
                            >> interact " "
                    (Just vars') -> do
                              val <- mapM (readTVarIO) vars'
                              print val
                              interact " "
                
              ("set":var:type':expr) -> do

                  let splits = splitOn "." var
                      portSetName = if length splits > 6
                                       then splits !! 5
                                       else ""
                      portName = if length splits > 6
                                    then splits !! 6
                                    else splits !! 5
                      sqn = SQN (ModuleName $ splits!!0)
                                (MeshName $ splits!!1)
                                (MeshInstanceName $ splits!!2)
                                (StructureName $ splits !! 3)
                                (StructureInstanceName $ splits !! 4)
                      fqn = toQualifiedName sqn
                                (PortSetName portSetName)
                                (PortName portName)
                      val = case type' of
                              "SBool" -> SBool $ read @Bool (intercalate " " expr)
                              "SInt" -> SInt $ read @Int (intercalate " " expr)
                              "SDouble" -> SDouble $ read @Double (intercalate " " expr)
                              --"SMaybe" -> SMaybe $ read (intercalate " " expr)
                              --"STuple" -> STuple $ read (intercalate " " expr)
                              -- "Anything" -> STuple $ read (intercalate " " expr)

                      cellId = (port_cell)!? fqn
                      mcells :: Maybe [TVarID]
                      mcells = ((cell_id)!?) =<< cellId
                      vars :: Maybe [TVar (Maybe SomeType)]
                      vars = mcells >>= (\cells ->
                                Just $ catMaybes $ map (id_var!?) cells) 
                 
                  modifyTVarIO gs (\globalState ->
                      globalState { rt_inputs 
                        = insert sqn val `into` (rt_inputs globalState) })
                  {-
                  case vars of
                    Nothing -> return ()
                    Just vars' -> do 
                        atomically $ mapM (\v -> writeTVar v val) vars'
                        return ()
                  -}
                  interact " "
              a -> do
                    putStrLn $ "did not recognize command;\n"
                             ++ (show a) ++ "\n"
                             ++"type help to see the available commands"
                    interact "x"
        

-- | create the runtime representation for a mesh:
-- a bunch of threads corresponding to the propagators used 
-- hooked up to a couple of TVars that "connect the ports".
-- "connecting ports" meaning that if propagatorInstance A port 1 
-- is linked to propagatorInstance B port 2, A.1 will 
-- point to TVar x and B.2 will point to TVar x as werll.
-- 
-- more techincally concerning the RuntimeState changes
-- sets rt_threads and rt_port_cell_mapping
createThreadsFor :: (TVar RuntimeState) -> MeshQualifiedName -> PropSpec -> Mesh -> IO ()
createThreadsFor globalState mqn spec mesh = do
    -- rule out stuff that is not implemented yet
    when (length (mesh_lambdaVars mesh) /= 0) $ do
        putStrLn $ "λ-Mesh not yet supported in mesh "
                 ++ (mesh_name mesh) ++ "' in module "
                 ++ (moduleName spec)
        exitWith $ ExitFailure 4
    when (isJust (mesh_extension mesh)) $ do
        putStrLn $ "ε-Mesh not yet supported in mesh "
                 ++ (mesh_name mesh) ++ "' in module "
                 ++ (moduleName spec)
        exitWith $ ExitFailure 4
  
    (!a,!b) <- runStateT (implement mqn $ mesh_definition mesh) 
            
             (IS globalState spec mesh 
                -- for now:
                (mesh_name mesh) 
                (fromList []) (fromList []))
    putStrLn $ "finished creating threads"


-- | see 'createThreadsFor';
-- implements the mesh by interpreting it line by line and constructing
-- the necessary TVars, Threads and TVar Mappings.
-- State is used to keep in mind the previously defined propagator instances
-- to properly wire them up
implement :: MeshQualifiedName -> [SingleSpecification] -> StateT ImplementationState IO ()
implement mqn [] = return ()
implement mqn !(spec:rest) = do
        gsv <- gets globalState
        gs <- readTVarIO gsv
        gspec <- gets globalSpec
        bindings' <- gets bindings
        meshName' <- gets (mesh_name . mesh)

        let prim = rt_prim gs
            props = propagators gspec
        case spec of
            (LetExpr var expr) -> do
                -- create an instance of a propagator
                
                spec <- gets globalSpec
                let sqn = toStructQualifiedName mqn
                              (StructureName $ unsafeFromLeft $ head expr)
                              (StructureInstanceName $ var)
                
                -- check whether the let binding is a propagator instantiation (then part)
                -- or simple assignment (else part)
                if isLeft (head expr)
                   && (unsafeFromLeft $ head expr) `member` props
                   then do
                        -- if it is a prop instantiation
                        -- add the instance to the state for later reference
                        modify (\is -> is 
                           { propVars = insert var (unsafeFromLeft $ head expr
                                                   , tail expr)
                                            `into` (propVars is) })
                        
                        -- check whether the propagator to construct was defined
                        case (propagators gspec)!?(unsafeFromLeft $ head expr) of
                            Nothing -> lift $ do
                                putStrLn $ "'" ++ (unsafeFromLeft $ head expr)
                                         ++"' not found"
                                exitWith $ ExitFailure 4


                            (Just prop) -> do
                                -- if so, create an instance
                                lift $ createThreadFor 
                                            gsv 
                                            sqn
                                            prop
                                            (map unsafeFromLeft 
                                            $ filter isLeft $ tail expr)
                                lift $ putStrLn $ "implemented '" ++ var ++ "'"
                   else do
                       -- if it is an assignment
                       -- execute the assignment
                       executed <- lift $ prim $ replaceBinds expr bindings'
                       -- put it into scope
                       modify (\is -> is
                         { bindings = insert var executed 
                                        `into` (bindings is) })

            (BindExpr var bound ports) -> lift $ do
                    putStrLn $ "Binding ports in mesh specifications "
                             ++"is not supported right now" 
                    exitWith $ ExitFailure 4
                    
            (WireExpr ops) -> implementWire ops

        -- recursively implement the rest of the mesh
        implement mqn rest

-- | implements a wiring diagram by constructing the proper propagator instance 
-- port to TVar mappings in the global state
implementWire :: [WireOp] -> StateT ImplementationState IO ()
implementWire = \case
    [] -> return ()
    [_] -> return ()
    ((TrivialOp l):(PortAccess r port):t) -> do 
        (cid, tid) <- createCells
        -- add mapping for left    
        registerWireComponent 
            (TrivialOp l) 
            cid tid
        -- add mapping for Right 
        registerWireComponent 
            (PortAccess r port) 
            cid tid
        -- setting state done
        lift $ putStrLn $ "wired " ++ l ++ " to " ++ r ++ "!" ++ port
        implementWire $ (PortAccess r port):t
        
    ((PortAccess l port):(TrivialOp r):t) -> do 
        (cid, tid) <- createCells
        -- add mapping for left    
        registerWireComponent 
            (TrivialOp r) 
            cid tid
        -- add mapping for Right 
        registerWireComponent 
            (PortAccess l port) 
            cid tid
        -- setting state done
        implementWire $ (TrivialOp r):t

        lift $ putStrLn $ "wired " ++ l ++ "!" ++ port ++  " to " ++ r
    (a:b:_) -> lift $ do
        putStrLn $ "Only Trivial-PortAccess or PortAccess-Trivial "
                 ++"wires are supported right now.\n"
                 ++(show [a,b])
        exitWith $ ExitFailure 4

-- | creates a cell and an accompanying TVar the cell points to 
-- in the global state
createCells :: StateT ImplementationState IO (CellID, TVarID)
createCells = do
        -- create a TVar for the wire
        tvar <- lift $ newTVarIO Nothing
        -- create the TVars id 
        tvarID <- lift $ nextRandom
        -- create a cellid
        cellID <- lift $ nextRandom
         
        
        -- add TVAR to runtime
        impState <- get
        lift $ modifyTVarIO (globalState impState) 
           (\gs -> gs { rt_tvars =
                                    insert (TID (show tvarID))
                                           tvar
                                       `into` (rt_tvars gs) })
        --lift $ print $ "created cell " ++ show cellID 
        --                ++ " with " ++ show tvarID
        
        return (CID cellID, TID $ show tvarID)

-- | replaces variables by current values
replaceBinds :: [Either String ExStrLit] 
             -> Map String SomeType
             -> [Either String StrLit]
replaceBinds words bindings 
    = foldr (flip (\acc -> \case
                (Left s) -> Left s : acc
                (Right (ExStrLit str vars)) ->
                    (Right 
                    $ StrLit 
                    $ foldr 
                        (\var acc -> 
                            replaceOne var 
                                        (printST $ fromMaybe 
                                                        (Anything "undefined")
                                                        (bindings!?var))
                                         acc) 
                        str vars)
                    : acc)) [] words

-- | replaces all occurrences of needle by replacement in haystack
replaceAll :: String -> String -> String -> String
replaceAll _ _ "" = ""
replaceAll needle replacement haystack
  | needle == (take (length needle) haystack) 
        = replacement ++ replaceAll needle replacement 
                            (drop (length needle) haystack)
  | otherwise 
        = head haystack : replaceAll needle replacement (tail haystack)

-- | replaces one occurrence of needle by replacement in haystack
replaceOne :: String -> String -> String -> String
replaceOne _ _ "" = ""
replaceOne needle replacement haystack
  | needle == (take (length needle) haystack) 
        = replacement ++ drop (length needle) haystack
  | otherwise 
        = head haystack : replaceOne needle replacement (tail haystack)



-- | connect a wire operand to the proper cell and tvar in the global state.
registerWireComponent :: WireOp -> CellID -> TVarID -> StateT ImplementationState IO ()
                      -- will connect the first port of the propagator to the cell
registerWireComponent (TrivialOp l) cellID tvarID = do
        impState <- get
        gs <- lift $ readTVarIO $ globalState impState

        let 
            propName' = fst <$> (propVars impState)!?l 
            moduleName' = moduleName $ globalSpec impState
            currentProp :: Maybe Propagator
            currentProp = ((propagators $ globalSpec impState)!?) =<< propName'
            firstPortSpec :: Maybe PortSet
            firstPortSpec = (headMaybe . prop_ports) =<< currentProp
            firstPortSetName :: Maybe String
            firstPortSetName = (Just . fromMaybe "" . portName) =<< firstPortSpec
            firstPort :: Maybe String
            firstPort = (headMaybe . ports) =<< firstPortSpec

                                 
        gs <- lift $ readTVarIO $ globalState impState
        meshName' <- gets (mesh_name . mesh)
        meshInstanceName' <- gets mesh_instance_name

        let 
            old_rt_cell = case (rt_cells gs !? cellID) of
                            (Nothing) -> []
                            (Just as) -> as
            mfqn = (\pn fpsn fp -> FQN (ModuleName moduleName')
                      (MeshName meshName')
                      (MeshInstanceName meshInstanceName')
                      (StructureName pn)
                      (StructureInstanceName l)
                      (PortSetName fpsn)
                      (PortName fp)) 
                        <$> propName' <*> firstPortSetName <*> firstPort

        case mfqn of
            Nothing -> lift do
                putStrLn $ "Could not register Trivial Component "
                         ++"'" ++ l ++ "'.\n"
                         ++"Structure:   " ++ (show propName') ++"\n"
                         ++"PortSetName: " ++ (show firstPortSetName) ++ "\n"
                         ++"PortSpec:    " ++ (show firstPortSpec) ++ "\n"
                         ++"FirstPort:   " ++ (show firstPort)
                exitWith $ ExitFailure 4
            (Just fqn) -> lift 
                $ modifyTVarIO (globalState impState)
                            (\gs -> gs
                                { rt_port_cell_mapping 
                                    = insert fqn
                                             cellID
                                        `into` (rt_port_cell_mapping gs) 
                                , rt_cells 
                                    = if (cellID `member` (rt_cells gs))
                                         && (tvarID `elem` 
                                                (rt_cells gs ! cellID))
                                         then insert cellID
                                                 (tvarID : (rt_cells gs ! cellID))
                                                `into` (rt_cells gs) 
                                         else rt_cells gs
                                })

                      -- port of propagator r will be connected to the cell
registerWireComponent (PortAccess r port) cellID tvarID = do
        impState <- get
        gs <- lift $ readTVarIO $ globalState impState

        let 
            (PortSetName psn,PortName pn) = fromPortString port
            propName' = fst <$> (propVars impState)!?r 
            moduleName' = moduleName $ globalSpec impState
            currentProp :: Maybe Propagator
            currentProp = ((propagators $ globalSpec impState)!?) =<< propName'
            matchPortSpec :: Maybe PortSet
            matchPortSpec = (headMaybe 
                            . filter ((pn `elem`) . ports) 
                            . filter ((==(Just psn))
                                        . portName)
                            . prop_ports) =<< currentProp
            matchPortSetName :: Maybe String
            matchPortSetName = (Just . fromMaybe "" . portName) =<< matchPortSpec

                                 
        gs <- lift $ readTVarIO $ globalState impState
        meshName' <- gets (mesh_name . mesh)
        meshInstanceName' <- gets mesh_instance_name
    
        let 
            old_rt_cell = case (rt_cells gs !? cellID) of
                            (Nothing) -> []
                            (Just as) -> as
            mfqn = (\pn fpsn fp -> FQN (ModuleName moduleName')
                      (MeshName meshName')
                      (MeshInstanceName meshInstanceName')
                      (StructureName pn)
                      (StructureInstanceName r)
                      (PortSetName fpsn)
                      (PortName fp)) 
                        <$> propName' <*> matchPortSetName <*> (Just pn)
        case mfqn of
            Nothing -> lift do
                putStrLn $ "Could not register PortSpec Component "
                         ++"'" ++ r ++ "'.\n"
                         ++"Structure:   " ++ (show propName') ++"\n"
                         ++"PortSetName: " ++ (show matchPortSetName) ++ "\n"
                         ++"PortSpec:    " ++ (show matchPortSpec) ++ "\n"
                         -- ++"CurrentProp: " ++ (show currentProp) ++ "\n"
                         ++"FirstPort:   " ++ (show pn)
                exitWith $ ExitFailure 4

            (Just fqn) -> do
                let newGS = gs 
                        { rt_port_cell_mapping 
                            = insert fqn
                                     cellID
                                 `into` (rt_port_cell_mapping gs) 
                        , rt_cells 
                            = insert cellID
                                     (tvarID : old_rt_cell)
                                  `into` (rt_cells gs) }

                lift $ writeTVarIO (globalState impState)
                                     newGS

-- | creates a reactive querying thread for a single propagator.
createThreadFor :: (TVar RuntimeState) -> StructureQualifiedName -> Propagator -> [String] -> IO ()
createThreadFor globalState sqn prop vars = do
    -- invariant checking
    when (length vars /= length (prop_lambdaVars prop)) $ do
        putStrLn $ "wrong number of arguments to λ-propagator '"
                ++ (prop_name prop) ++ "'.\n"
                ++ "expected " ++ (show $ length $ prop_lambdaVars prop) 
                ++ " but got " ++ (show $ length vars)
        exitWith $ ExitFailure 3
    when (isJust $ prop_extension prop) $ do
        putStrLn $ "Unsupported Language Feature:\n"
                ++ "Extensions are not supported yet in ε-propagator '"
                ++ (prop_name prop) ++ "'."
        exitWith $ ExitFailure 4

    -- the unique name
    myID <- UUID.nextRandom
    
    -- tracking what the cell contents were in the last check
    lastChanges <- newTVarIO (fromList [])

    -- forking off the thread
    threadID <- forkIO $ thread myID lastChanges prop vars

    -- adding the thread to the global runtime state for reference
    modifyTVarIO globalState 
        (\gs -> gs { rt_threads = insert sqn threadID
                                    `into` (rt_threads gs) })

    putStrLn $ "created thread for " ++ showSQN sqn 
        where 
            -- | actually creates the thread a propagator will run as 
            -- the thread queries the contents of the matched input ports 
            -- and compares them to the last seen contents. 
            -- if changes are seen the propagator implementation code is invoked
            -- with the newly seen changes;
            -- then the lastChanges are updated 'thread' recursively calls itself
            --
            -- the special 'in' propagator that listens for changes done manually via 
            -- the repl is also implemented here
            -- TODO: not sure if this special handling here is efficient;
            -- another approach would be favourable where the code can be optimized better
            thread :: UUID
                   -> TVar (Map FullyQualifiedName (Map TVarID SomeType))
                   -> Propagator
                   -> [String]
                   -> IO ()
            thread constrUUID lastChanges
                    prop@(Propagator name _ _ _ (Right (Left pms)) _) lambdaVars = do
               
                -- the special 'in' propagator:
                if (name == "in")
                    then do
                        (RS _ port_cell cell_ids id_tvar _ inputs) <- readTVarIO globalState
                        --putStrLn $ showSQN sqn ++ " asking for inputs"
                        
                        -- wait until inputs from the repl were added to the global state
                        if sqn `member` inputs
                           then do 
                                -- " === found inputs === "
                                -- parse the cell name
                                let outcellName = toQualifiedName sqn 
                                                (PortSetName "O")
                                                (PortName "_1")
                                    outCell = port_cell !? outcellName
                                    ids = (cell_ids !?) =<< outCell
                                    mayVars=(sequence . map (id_tvar!?)) =<< ids

                                case mayVars of
                                    Nothing -> do
                                        putStrLn $ " missing some vars:\n"
                                                 ++(showFQN outcellName)++"\n"
                                                 ++(show outCell) ++ "\n"
                                                 ++(show ids) ++ "\n"
                                                 ++"moving on"
                                    (Just vars) -> do
                                        -- set the contents of all TVars the cell points to 
                                        -- to the desired contents
                                        atomically $ do
                                            mapM_ 
                                              (\v -> writeTVar v (Just $ inputs!sqn)) 
                                              vars
                                            modifyTVar globalState (\rs ->
                                                rs{rt_inputs = 
                                                    M.delete sqn (rt_inputs rs) 
                                                  })
                                
                           else return ()
                    else do
                        -- waits until a change was detected and executes the change
                        _ <- sequenceUntil (==True)
                            $ map 
                                (\pm -> execPatternMatch 
                                            globalState 
                                            lastChanges 
                                            sqn
                                            prop
                                            pm
                                            lambdaVars)
                                pms
                        return () 
                
                yield
                -- the clock
                threadDelay 200 --250000
                thread constrUUID lastChanges prop lambdaVars
            thread _ _ _ _ = do
                putStrLn $ "Unsupported Language Feature:\n"
                        ++ "Extensions are not supported yet in ε-propagator '"
                        ++ (prop_name prop) ++ "'."
                exitWith $ ExitFailure 4


-- | retrieves the first element that satisfies a predicate
firstBy :: (a -> Bool) -> [a] -> (Maybe a)
firstBy f [] = Nothing
firstBy f (h:t) = 
    if f h 
       then Just h
       else firstBy f t

-- | sequences a monadic action until its result fulfils a predicate
sequenceUntil :: Monad m => (a -> Bool) -> [m a] -> m (Maybe a)
sequenceUntil f [] = return Nothing
sequenceUntil f (h:t) = do
    res <- h
    if f res 
       then return $ Just res
       else sequenceUntil f t

-- | parser for qualified names
fromPortString :: String -> (PortSetName, PortName)
fromPortString s = (PortSetName portSetName, PortName portName)
    where
        splits = ("." `splitOn` s)
        portSetName = if length splits > 1
                         then splits !! 0
                         else ""
        portName = if length splits > 1
                         then splits !! 1
                         else splits !! 0



-- | executes an IO operation when no changes to the listener variables were detected
-- or an IO operation that takes the names of the variables and their corresponding TVars 
-- as input if changes were detected in these variables.
execRegardlessChangedWith :: TVar RuntimeState
                 -> [String]
                 -> TVar (Map FullyQualifiedName (Map TVarID SomeType))
                 -> StructureQualifiedName
                 -> IO Bool
                 -> ([FullyQualifiedName] -> [(FullyQualifiedName, [TVarID])] 
                        -> IO Bool)
                 -> IO Bool 
execRegardlessChangedWith globalState listenVars lastChanges sqn falseAction trueAction = do
            let fqns = map (\v -> let 
                                    (psn,pn) = fromPortString v
                                  in
                                  toQualifiedName sqn psn pn
                                            
                           ) listenVars 
            changed <- mapM (\fqn -> do
                                hc <- hasChanged globalState lastChanges fqn
                                return (fqn, hc))
                            fqns
            {-
            if(sqn_structureInstanceName sqn == (StructureInstanceName "lba"))
               then print $ "lba changed: " ++ (show $ 
                                map (\(a,b) -> (showFQN a,b)) changed)
               else return ()
               -}
            if (any ((==[]) . snd) changed)
               then falseAction             
               else trueAction fqns changed

onChanged = ($)
onNoChange = ($)
into = ($)

-- | execPatternMatch basically works like this: 
-- get all patternMatches
-- for each pattern match
--      get all vars from globalState
--        (short circuit if not available)
--      compare patternMatches:
--        check pattern match
--          (if variable in propspec
--             then 
--               if value in variable is different
--                  than my lastChanges
--               then 
--             else 
--          )
--          if all vars for match true
--              then 
--                  execute code
--                  && set lastChanges 
--              else short circuit
execPatternMatch :: TVar RuntimeState
                 -> TVar (Map FullyQualifiedName (Map TVarID SomeType))
                 -> StructureQualifiedName
                 -> Propagator
                 -> PatternMatch
                 -> [String]
                 -> IO Bool
execPatternMatch globalState lastChanges sqn prop (PM listenVars prog) lambdaVars
        = execRegardlessChangedWith globalState listenVars lastChanges sqn
                `onNoChange` (return False)
                `onChanged` (\fqns changed ->
                     executeProgram 
                        globalState 
                        lastChanges 
                        prop 
                        fqns 
                        changed
                        sqn 
                        prog 
                        lambdaVars
                      >> return True)

execPatternMatch globalState lastChanges sqn prop (Guards (Guard listenVars filters progs)) lambdaVars
        = do 
        execRegardlessChangedWith globalState listenVars lastChanges sqn
            `onNoChange` (return False)
            `onChanged`  (\fqns changed -> do
                -- pick first matching guard pattern
                results' <- runMaybeT $ mapM (\(f,p) -> do 
                                res <- execute globalState lastChanges 
                                            sqn prop f lambdaVars
                                return (res, p)
                            ) $ zip filters progs
                                
                -- and return the program associated with it
                case results' of
                  -- if no values were found for the variables to check;
                  -- do not execute
                  Nothing -> return False
                  (Just results) -> do
                        let prog = snd 
                                 <$> ( pickFirst ((== (SBool True)) . fst) 
                                         $ results)
                        case prog of
                          Nothing -> return False
                          Just p -> do
                            -- execute program(s)
                            executeProgram globalState lastChanges
                                            prop fqns 
                                            changed
                                            sqn p lambdaVars
                            return True

            )

-- | interprets the implementation ("the program") of a propagator
executeProgram :: TVar RuntimeState 
               -> TVar (Map FullyQualifiedName (Map TVarID SomeType))
               -> Propagator -> [FullyQualifiedName] 
               -> [(FullyQualifiedName,[TVarID])] -> StructureQualifiedName 
               -> [OutputSpec]-> [String] -> IO ()
executeProgram globalState lastChanges prop 
            vars lastChangedVars sqn p lambdaVars = 
        -- execute program(s)
        mapM_ (\os -> do
            (RS _ port_cell cells_cellid cellid_tvars _ _) <- readTVarIO globalState
            let 
                fqns :: [FullyQualifiedName]
                fqns = map (\n -> let 
                                    (psn,pn) = fromPortString n
                                    in
                                    toQualifiedName sqn psn pn)
                            $ osNames os
                mcellNames :: Maybe [CellID]
                mcellNames = sequence $ (map (port_cell!?)) fqns
                mcellsDefined :: Bool
                mcellsDefined = fromMaybe False 
                             $ (all (==True) .
                                map (\n ->
                                    n `member` cells_cellid)) <$> mcellNames
            
                writeOut = (fqn_portName $ head fqns) == PortName "__"
            case (writeOut, mcellNames,mcellsDefined) of
              (False, Nothing, _) -> do 
                    putStrLn $ "Cell names were not found in "
                             ++ (showSQN sqn) ++ "\n"
                             ++ (show $ map showFQN fqns) ++ "\n"
                             ++ (show mcellNames)
                    exitWith $ ExitFailure 4
              (_, Just cellNames, False) -> 
                    return ()
                
              -- if the port name of the output port is '__'
              (True, Nothing, _) -> do
                    -- the output of the interpretation is ignored;
                    -- useful for using print like commands of the SIMBA-prelude
                    val <- runMaybeT $ execute globalState lastChanges 
                                sqn prop (osExpr os) lambdaVars

                    setLastChanges globalState lastChanges lastChangedVars
                
              -- if the variables are defined and available
              (False, Just cellNames, True) -> do
                    out_vars <- intercalate []
                                  <$> mapM (resolveCell globalState) cellNames

                    val' <- runMaybeT $ execute globalState lastChanges 
                                sqn prop (osExpr os) lambdaVars
                    case val' of
                      Nothing -> return ()
                      (Just val) -> do
                        -- set output values
                        mapM_ (\var -> writeTVarIO var (Just val))
                              out_vars
                        
                        setLastChanges globalState lastChanges lastChangedVars
                            
        ) p

-- | resets the last changes for a specific cell to the values specified value
setLastChanges :: TVar RuntimeState
               -> TVar (Map FullyQualifiedName (Map TVarID SomeType)) 
               -> [(FullyQualifiedName, [TVarID])] -> IO ()
setLastChanges globalState lastChanges lastChangedVars= do
    varsWithVals <- mapM (\(fqn,tvarIDs) -> atomically $ do
            (RS _ _ _ id_var _ _) <- readTVar globalState
            varsNVals <- mapM (\var -> do
                                case (id_var !? var) of
                                  Nothing -> return (var, Nothing)
                                  (Just var') -> do
                                    val <- readTVar var'
                                    return (var,val)
                              ) tvarIDs
            return (fqn,varsNVals)
        ) lastChangedVars

    modifyTVarIO lastChanges 
        (\lcm ->
            foldr (\(var,valMap) acc ->
                    let newMap = foldr 
                            (\(varid,val) m ->
                                case val of
                                  Nothing -> m
                                  (Just val') ->
                                    insert varid val' 
                                        `into` m)
                            (fromMaybe (fromList []) $ acc !? var)
                            valMap
                     in 
                    insert var newMap `into` acc) 
                lcm 
                varsWithVals
        )

-- | executes the implementation of a propagator on an assignment 
-- by replacing variables and then handing off the command to the prim function
-- (the SIMBA prelude)
execute :: TVar RuntimeState 
        -> TVar (Map FullyQualifiedName (Map TVarID SomeType))
        -> StructureQualifiedName
        -> Propagator
        -> [Either String ExStrLit] 
        -> [String] 
        -> MaybeT IO SomeType
execute globalState lastChanges sqn prop program lambdas = do
    (RS threads port_cell cells vars prim _) <- lift $ readTVarIO globalState
    lc <- readTVarIO lastChanges
    valuesReplaced <- replaceVars globalState lc
                                    sqn prop program lambdas
    
    lift $ prim valuesReplaced

-- | replaces variables (qualified names) by the actual value
replaceVars :: TVar RuntimeState   
                 -> Map FullyQualifiedName (Map TVarID SomeType)
            -> StructureQualifiedName -> Propagator -> [Either String ExStrLit] -> [String] -> MaybeT IO [Either String StrLit]
replaceVars globalState lastChanges sqn prop@(Propagator _ lambdaVars _ ports _ _) 
            program lambdas = do
                noStrLits <- removeStringLits program 
                replacedLambdas <- return $ foldr (\(l,v) acc -> 
                                    replaceLambdas l v acc
                                    ) noStrLits $ zip lambdaVars lambdas
                --lift $ print $ "replaced lambdas: " ++ show replacedLambdas
                foldrM (\l acc -> replace False l acc) replacedLambdas vars
        where 
            vars = intercalate [] 
                 $ map (\(PortSet n ps) -> 
                            map (\pn -> toQualifiedName sqn
                                          (PortSetName $ fromMaybe "" n)
                                          (PortName pn)) ps
                        ) ports

            removeStringLits :: [Either String ExStrLit] -> MaybeT IO [Either String StrLit]
            removeStringLits strs 
                = foldrM (\s acc ->
                        case s of
                          Left s' -> return $ (Left s') : acc
                          Right (ExStrLit s' vars) -> do

                              replaced <- foldrM (\var s_acc -> do
                                    let 
                                        (psn,pn) = fromPortString (var)
                                        fqn = toQualifiedName sqn 
                                                psn
                                                pn

                                    rep <- replace True
                                            fqn
                                            (map Left $ splitOn " " s_acc)
                                    return $ intercalate " "
                                           $ map unsafeFromLeft
                                           $ rep
                                     ) s' vars 
                              return $ (Right $ StrLit replaced) : acc
                        ) [] strs

               
            replaceLambdas :: String -> String -> [Either String StrLit]
                           -> [Either String StrLit]
            replaceLambdas needle replacement [] = []
            replaceLambdas needle replacement (Left h:t)
                | h == needle = Left replacement :
                                  (replaceLambdas needle replacement t)
                | otherwise = Left h : (replaceLambdas needle replacement t)
            replaceLambdas needle replacement (Right (StrLit h):t) 
                = (Right $ StrLit $ replaceAll ('%' : needle) replacement h)
                    : (replaceLambdas needle replacement t)
            


            matches :: FullyQualifiedName -> String -> Bool
            (FQN mod meshName meshInstance structureName structureInstance
                 (PortSetName portSetName) (PortName portName)) `matches` str
              | portName == str
              || '%':portName == str
              || portSetName ++"."++ portName == str 
              || "%" ++ portSetName ++ "." ++ portName == str = True
              | otherwise = False

            replace :: Bool -> FullyQualifiedName -> [Either String StrLit] -> MaybeT IO [Either String StrLit]
            replace _ _ [] = return []
            replace instr a (Right h: t) = do
                next <- replace instr a t
                return $ Right h : next
            replace instr a (Left h:t) 
              | a `matches` h   = do
                  cellContents <- getLatestValue instr a 
                  next <- replace instr a t
                  return ((Left $ printST cellContents) : next)
              | otherwise = do
                  next <- replace instr a t 
                  return (Left h : next)

            getLatestValue :: Bool -> FullyQualifiedName -> MaybeT IO SomeType
            getLatestValue instr n = do
                  (RS _ port_cellid cell_tvarids tvarid_tvar _ _) <- readTVarIO globalState
                  --lift $ print $ "getting last changed val for " ++ ( showFQN n)
                  --
                  cellID <- hoistMaybe $ port_cellid !? n
                  tvarIDs <- hoistMaybe $ cell_tvarids !? cellID
                  --lift $ print $ "tvarID: " ++ (show tvarIDs)

                  currentValueMap <- mapM (\tid -> do
                                            var <- hoistMaybe $ tvarid_tvar !? tid
                                            val <- readTVarIO var
                                            return (tid,val)) tvarIDs
                  if instr
                    then do
                      -- TODO: semantics correct?
                      val <- hoistMaybe $ join $ 
                                snd <$> headMaybe currentValueMap
                      return val
                    else do
                      -- save usage of (!)
                      lastChangedVal <- hoistMaybe 
                                      $ join 
                                      $ firstBy isJust $ map  (\(tid,value) ->
                                    if (n `member` lastChanges)
                                       && tid `member` (lastChanges ! n)
                                       && ((Just $ lastChanges ! n ! tid) == value )
                                         then Nothing
                                         else value
                                        ) currentValueMap
                      return lastChangedVal 


-- | determines if a cell value has changed  since the last setting of lastValue
hasChanged :: TVar RuntimeState 
           -> TVar (Map FullyQualifiedName (Map TVarID SomeType))
           -> FullyQualifiedName
           -> IO [TVarID]
hasChanged globalState lc qualifiedName = do
    (RS threads port_cell cells vars prim _) <- readTVarIO globalState

    -- when the name was not bound to a cell yet
    let 
        mcellID = port_cell !? qualifiedName
        nameBound = qualifiedName `member` port_cell
        cellCreated = fromMaybe False $ (`member` cells) <$> mcellID
        cellConnected = fromMaybe False $ join $ (((/=[]) <$>). (cells !?)) <$> mcellID
        checks = [nameBound, cellCreated, cellConnected]
   
    case (mcellID,all (==True) checks) of
      (Nothing, _) -> do
          --print "here:" 
          --print $ showFQN qualifiedName
          --pPrint port_cell
          return [] 
      (_, False) -> do
          --print "here2"
          return []
      
      -- from here on; all usage of (!) is safe because of the valid checks
      (Just cellID, True) -> do
        -- otherwise check if any has changed
        --print "changing"
        results <- catMaybes <$> 
                 ( fromMaybe (return []::IO [Maybe (Bool, TVarID)]) 
                 $ (mapM (\cell -> runMaybeT @IO $ do
                            cellVar <- hoistMaybe $ vars !? cell
                          
                            cellContents <- readTVarIO cellVar
                            lastChanges <- readTVarIO lc
                            let lastChangeVal = (!? cell) 
                                            <$> (lastChanges !? qualifiedName)
                            
                            if (isNothing lastChangeVal)
                                -- when cell has not been written to 
                                then return (True,cell)
                                else if (lastChangeVal == Just cellContents)
                                    then return (False,cell)
                                    else return (True,cell)
                         ) 
                    <$> cells !? cellID))
        --print "done"
            {-
        if (fqn_structureInstanceName qualifiedName
            == (StructureInstanceName "lba"))
           then print $ "changes: " ++ show results
           else return ()
           -}
        return $ map snd $ filter ((==True) . fst) results

pickFirst :: (a -> Bool) -> [a] -> Maybe a
pickFirst f = headMaybe . dropWhile (not . f)

headMaybe :: [a] -> Maybe a
headMaybe [] = Nothing
headMaybe (h:t) = Just h 

unsafeFromLeft :: Either a b -> a
unsafeFromLeft (Left a) = a
unsafeFromLeft _ = error "Right fromLeft"
