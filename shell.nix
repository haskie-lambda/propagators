{ pkgs ? import <nixpkgs> {} }:
let myghc = pkgs.haskellPackages.ghcWithPackages (hpkgs: [hpkgs.pretty-simple]);
in 
  pkgs.mkShell {
      nativeBuildInputs = [ 
          myghc
    ];
}
