module Main where
import Prop.Lexer
import Prop.Parser
import Prop.Language
import Control.Monad.Trans.State.Strict
import Text.Pretty.Simple (pPrint)
import Data.Map (fromList)
import Prop.Language.PRIM (primitives)

main = do
    s <- readFile "./testfiles/simple.prop"
    --mapM_ print (alexScanTokens s)
    --print "---"
    --pPrint $ fst $ runState (parse (alexScanTokens s)) initialState
    parsed <- return $ fst $ runState (parse (alexScanTokens s)) initialState
    putStrLn "parsed... \ninterpreting..."
    run primitives (fromList []) parsed
