presentation notes
------------------

Simba
Simulated aBstract hArdware

## Idea
what we have seen:
- imperative paradigm abstract machines
- functional paradigm abstract machines
- logical paradigm abstract machiens

what about: 
- concurrent-
- reactive-
- dataflow-
paradigm abstract machines?

implement a propagator/hardware simulation language 
where paralellism, metaprogramming, and extensability
are key parts 

## Propagators (and why they are interesting)

- what is a propagator?
- motivation, why is this nice
- example program, back and forward application
- similar languages? (hdl, vhdl, verilog)

## Implentation
- haskell

- alex
- happy

- stm

every instance of propagator is compiled to 
a thread running in a loop waiting for changes
executing if necessary

meshes specify what propagator-instances there are 
and how they are connected 

connections between ports are called cells
having the same cell is the definition of being
connected.


it is not a stack or register machine.
execution of code in propagator works entirely
by substitution.

there is a global state that contains every 
bit of information of every cell in a map/table

## Features 

full reactive: 
listeners extremely easy to implement

program is a haskell datatype,
haskell programs can produce simba programs
--> large networks become feasable.

foreign function interface easily achievable
by adding it to the simba prelude

every cell can be changed by the system user 
during runtime

## limitations
right now
- only one "main" mesh is supported
- the language prelude library is really small
- implementation of metaprogarmming was started but 
  is not finished yet
  >> language extensions are unsupported yet
- haskell runtime is responsible for distributing 
  the threads accordingly
- clock resolution can't be set yet, but very soon
- no type system
  (although it is questionable how to implement it)
- no module system

## Planned work
remove limitations
adding builtin support for join semilattices in cells
use simba to write an ad-hoc type-system for 
haskell or a constraint solver

## other
introduction to propagators https://www.youtube.com/watch?v=nY1BCv3xn24

